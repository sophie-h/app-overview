const APP_ID = 2;
const API_URL = 'https://apps.gnome.org/_api/apps.json';

const div_table = document.getElementById('table');
const div_wrapper = document.getElementById('wrapper');

function on_checkbox_change(checkbox, default_state) {
    console.log(checkbox.checked);
    const params = new URLSearchParams(window.location.search);

    if (checkbox.checked == default_state) {
        params.delete(checkbox.id);
    } else {
        params.set(checkbox.id, checkbox.checked ? "true" : "");
    }

    history.replaceState({}, "", `${window.location.pathname}?${params}`);
}

function on_search(input) {
    console.log(input.value);
    const params = new URLSearchParams(window.location.search);

    if (input.value === "") {
        params.delete("SEARCH");
    } else {
        params.set("SEARCH", input.value);
    }

    history.replaceState({}, "", `${window.location.pathname}?${params}`);
}

const search_params = new URLSearchParams(window.location.search);

for (const field of fields) {
    if ('heading' in field) {
        const h3 = div_table.insertBefore(document.createElement("h4"), div_wrapper);
        h3.textContent = field['heading'];
    }
    else if ('id' in field) {
        const checkbox = div_table.insertBefore(document.createElement("input"), div_wrapper);
        checkbox.setAttribute("type", 'checkbox');
        checkbox.setAttribute("id", field['id']);

        let default_state = Boolean(field['checked']);

        checkbox.checked = default_state;

        if (search_params.get(field['id']) !== null) {
            checkbox.checked = Boolean(search_params.get(field['id']));
        }

        checkbox.addEventListener('change', () => on_checkbox_change(checkbox, default_state));

        const label = div_table.insertBefore(document.createElement("label"), div_wrapper);
        label.setAttribute("for", field['id']);
        label.textContent = field['name'];

        if (field['description']) {
            const description = label.appendChild(document.createElement("small"));
            description.textContent = field['description'];
        }

        const style = div_table.insertBefore(document.createElement("style"), div_wrapper);
        style.textContent = `#${field['id']}:not(:checked) ~ * [data-column-id="${field['id']}"] { display: none !important; }`
    }
}

table_fields = fields.filter(field => 'name' in field)

let columns = [];

for (const field of table_fields) {
    let column = {
        name: field['name']
    };

    if ('id' in field) {
        column['id'] = field['id'];
    }

    if ('sort' in field) {
        column['sort'] = field['sort'];
    } else {
        // Use a sort that handles null and sorts it to the end
        column['sort'] = {
            compare: (a, b) => {
                console.log(a, b)
                if (a === null && b === null) {
                    return 0;
                } else if (a === null) {
                    return 1;
                } else if (b === null) {
                    return -1;
                } if (a > b) {
                    return 1;
                } else if (b > a) {
                    return -1;
                } else {
                    return 0;
                }
            }
        }
    }

    if ('formatter' in field) {
        column['formatter'] = field['formatter'];
    }

    columns.push(column);
}

let data_transform = [];

for (const field of table_fields) {
    if (field['data']) {
        data_transform.push(field['data']);
    } else if (field['id']) {
        const id = field['id'];
        data_transform.push(app => {
            if (id in app) {
                return app[id]
            } else {
                return id
            }
        });
    } else {
        data_transform.push(app => "");
    }
}

const grid = new gridjs.Grid({
    columns: columns,
    server: {
        url: API_URL,
        then: data => Object.values(data).map(app => data_transform.map(transform => transform(app)))
    },
    search: {
        enabled: true,
        keyword: search_params.get("SEARCH") ?? "",
    },
    sort: true,
    resizable: true,
    width: "auto",
    autoWidth: true,
});

function build() {
    grid.render(document.getElementById("wrapper"));
}

build();
const search = document.getElementsByClassName('gridjs-search-input')[0];
search.addEventListener('input', () => {
    on_search(search)
});
