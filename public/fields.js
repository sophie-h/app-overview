const fields = [{
    name: '#',
    sort: false,
}, {
    heading: "Basics"
}, {
    id: 'gnome',
    name: 'App Category',
    checked: true,
}, {
    id: 'id',
    name: 'App ID',
}, {
    id: 'name',
    name: 'Name',
    checked: true,
    formatter: (cell, row) => gridjs.html(`<a href='https://apps.gnome.org/app/${row.cells[APP_ID].data}/'>${cell}</a>`)
}, {
    id: "repo_url",
    name: "Repo URL",
}, {
    id: "repo_name",
    name: 'Repo',
    formatter: (cell, row) => gridjs.html(`<a href='${row.cells[4].data}/'>${cell}</a>`)
}, {
    id: "programming_languages",
    name: "Programming Languages",
    formatter: (cell, _) => cell.join(", ")
}, {
    heading: "Flathub"
}, {
    id: "on_flathub",
    name: "On Flathub",
    formatter: (cell, _) => cell ? "Yes" : "No",
}, {
    id: "flathub_verified",
    name: "Flathub Verified",
    data: app => app.flathub_verified === null ? "–" : (app.flathub_verified ? "Yes" : "No"),
}, {
    id: "flathub_last_build",
    name: "Flathub Last Build",
    data: app => app.flathub_last_build,
}, {
    id: "flathub_stats_installs",
    name: "Installs/Day",
    description: "Averaged over last 6 months",
    formatter: (cell, row) => cell ? gridjs.html(`<a href="https://klausenbusk.github.io/flathub-stats/#ref=${row.cells[APP_ID].data}&downloadType=installs">${cell.toFixed(1)}</a>`) : ""
}, {
    id: "flathub_stats_updates",
    name: "Updated Installs",
    description: "Number of updates since last build. Indicator for number of existing installs that get updated.",
    formatter: (cell, row) => {
        if (cell) {
            return gridjs.html(`<a href="https://klausenbusk.github.io/flathub-stats/#ref=${row.cells[APP_ID].data}&downloadType=updates">${cell.toLocaleString('fr-FR')}</a>`);
        } else {
            return "–";
        }
    },
}, {
    heading: "Internanionalization"
}, {
    id: "languages",
    name: "#Languages",
    formatter: (cell, _) => cell.length
}, {
    id: "dl_module",
    name: "Damned Lies",
    description: "Module name on l10n.gnome.org",
    formatter: (cell, _) => cell ? gridjs.html(`<a href='https://l10n.gnome.org/module/${cell}/'>${cell}</a>`) : "",
}, {
    id: "dl_strings",
    name: "#Strings",
    description: "Number of translatable strings",
}, {
    id: "url_translate",
    name: "Translation URL",
    description: "URL explicitly specified in appstream",
    formatter: (cell, _) => cell ? gridjs.html(`<a href='${cell}'>${cell}</a>`) : "",
}, {
    heading: "Marketing"
}, {
    id: "summary",
    name: "Summary",
}, {
    id: "screenshots",
    name: "#Screenshots",
    formatter: (cell, _) => cell.length
}, {
    id: "brand_color_light",
    name: "Brand Color Light",
    formatter: (cell, _) => gridjs.html(`<div class="color" style="background: ${cell}">${cell}</div>`)
}, {
    id: "banner",
    name: "Banner",
    data: app => app.id,
    formatter: (cell, _) => gridjs.html(`<img width="260" src="https://apps.gnome.org/icons/twitter/${cell}.png"/>`)
}, {
    heading: "Health Indicators"
}, {
    id: "gtk_version",
    name: "GTK Version",
}, {
    id: "flathub_runtime",
    name: "Runtime",
}, {
    id: "latestversion",
    name: "Latest Version",
    data: app => (app.releases[0] || {
        version: ""
    }).version
}, {
    id: "lastrelease",
    name: "Last Release",
    data: app => {
        if (!app.releases || !app.releases[0] || !app.releases[0].date) {
            return "";
        } else {
            return app.releases[0].date.slice(0, 10);
        }
    }
}, {
    id: "members",
    name: "#Maintainers",
    formatter: (cell, _) => cell.length,
}, {
    id: "ords",
    name: "ODRS",
    description: "Link to reviews",
    formatter: (_, row) => gridjs.html(`<a href='https://odrs.gnome.org/1.0/reviews/api/app/${row.cells[APP_ID].data}'>odrs</a>`)
}, {
    id: "annotations",
    name: "Annotations",
    description: "Automatic messages about potential problems",
    formatter: (cell, _) => gridjs.html(cell.map(x => "<li>" + x.msg.replace(/</g, "&lt;").replace(/>/g, "&gt;") + "</li>").join("\n"))
}, {
    heading: "URLs"
}, {
    id: "url_donation",
    name: "Donation URL",
    formatter: (cell, _) => cell ? gridjs.html(`<a href='${cell}/'>${cell}</a>`) : "",
}, {
    id: "url_homepage",
    name: "Homepage URL",
    formatter: (cell, _) => cell ? gridjs.html(`<a href='${cell}/'>${cell}</a>`) : "",
}];
